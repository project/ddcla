<?php
/**
 * @file dcla_custom.pages.inc
 * Page callbacks for the dcla_custom module.
 */

/*
 * A modal user login callback.
 */
function dcla_custom_login($js = NULL) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('user_login');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Log in'),
    'ajax' => TRUE,
  );

  $output = ctools_modal_form_wrapper('user_login', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    $output[] = ctools_modal_command_dismiss(t('Login Success'));
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
  exit;
}

/**
 * A modal user register callback.
 */
function dcla_custom_register($js = NULL) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('user_register_form');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Create new account'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('user_register_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
  exit;
}

/**
 * A modal user password callback.
 */
function dcla_custom_password($js = NULL) {
  module_load_include('inc', 'user', 'user.pages');

  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('user_pass');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Request new password'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('user_pass', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  print ajax_render($output);
  exit;
}


/**
 * A sessions page callback
 */
function dcla_custom_sessions_page($node) {
 
$output = theme( 'sessions_page', array('node' => $node ));
drupal_add_js('sites/all/libraries/jquery-plugins/jquery.scrollTo.min.js', 'file');
	return array('markup' => array('#markup' => $output));
}

function dcla_custom_show_session( $js= NULL ){
	
	$node = node_load(arg(4));
	
	if ($js) {
	
	ctools_include('ajax');
    
    $commands = array();
   	$output = views_embed_view('sessions', 'block_single', $node->nid);			
	$commands[] = ajax_command_html('#single-session', $output);
	print ajax_render($commands); // this function exits.
	exit;
	
	}
	else{ return  drupal_goto('node/'.$node->nid);}
	
	
}
function dcla_custom_show_all_sessions( $js= NULL ){
	
	
	
	if ($js) {
	
	ctools_include('ajax');
    
    $commands = array();
   	$output = views_embed_view('sessions', 'block_2');			
	$commands[] = ajax_command_html('#sessions-list-inner', $output);
	print ajax_render($commands); // this function exits.
	exit;
	
	}
	else{ return  drupal_goto('sessions');}
	
	
}
function dcla_custom_show_user_session( $js= NULL ){
	
	global $user;
	
	if ($js) {
	
	ctools_include('ajax');
    
    $commands = array();
   	$output = views_embed_view('sessions', 'block_user_picked', $user->uid);			
	$commands[] = ajax_command_html('#user-sessions-list-inner', $output);
	print ajax_render($commands); // this function exits.
	exit;
	
	}
	else{ return  drupal_goto('sessions/picked') ;}
	
	
}
