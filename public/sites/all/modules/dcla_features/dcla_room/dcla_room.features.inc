<?php
/**
 * @file
 * dcla_room.features.inc
 */

/**
 * Implements hook_node_info().
 */
function dcla_room_node_info() {
  $items = array(
    'room' => array(
      'name' => t('Room'),
      'base' => 'node_content',
      'description' => t('Conference session room'),
      'has_title' => '1',
      'title_label' => t('Room name'),
      'help' => '',
    ),
  );
  return $items;
}
