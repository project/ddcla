<?php
/**
 * @file
 * slideshow_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function slideshow_content_type_node_info() {
  $items = array(
    'slideshow_content' => array(
      'name' => t('Slideshow Content'),
      'base' => 'node_content',
      'description' => t('Nodes published here will appear in home page slideshow'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
