<?php
/**
 * @file
 * dcla_session.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function dcla_session_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-sessions.
  $menus['menu-sessions'] = array(
    'menu_name' => 'menu-sessions',
    'title' => 'Session Categories',
    'description' => 'Session categories for sublists',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Session Categories');
  t('Session categories for sublists');


  return $menus;
}
