<?php
/**
 * @file
 * dcla_session.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dcla_session_user_default_permissions() {
  $permissions = array();

  // Exported permission: create field_session_day.
  $permissions['create field_session_day'] = array(
    'name' => 'create field_session_day',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_session_room.
  $permissions['create field_session_room'] = array(
    'name' => 'create field_session_room',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_session_status.
  $permissions['create field_session_status'] = array(
    'name' => 'create field_session_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_session_time.
  $permissions['create field_session_time'] = array(
    'name' => 'create field_session_time',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create session content.
  $permissions['create session content'] = array(
    'name' => 'create session content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any session content.
  $permissions['delete any session content'] = array(
    'name' => 'delete any session content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own session content.
  $permissions['delete own session content'] = array(
    'name' => 'delete own session content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete terms in 4.
  $permissions['delete terms in 4'] = array(
    'name' => 'delete terms in 4',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 5.
  $permissions['delete terms in 5'] = array(
    'name' => 'delete terms in 5',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: delete terms in 6.
  $permissions['delete terms in 6'] = array(
    'name' => 'delete terms in 6',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: edit any session content.
  $permissions['edit any session content'] = array(
    'name' => 'edit any session content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit field_session_day.
  $permissions['edit field_session_day'] = array(
    'name' => 'edit field_session_day',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_session_room.
  $permissions['edit field_session_room'] = array(
    'name' => 'edit field_session_room',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_session_status.
  $permissions['edit field_session_status'] = array(
    'name' => 'edit field_session_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_session_time.
  $permissions['edit field_session_time'] = array(
    'name' => 'edit field_session_time',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_session_day.
  $permissions['edit own field_session_day'] = array(
    'name' => 'edit own field_session_day',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_session_room.
  $permissions['edit own field_session_room'] = array(
    'name' => 'edit own field_session_room',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_session_status.
  $permissions['edit own field_session_status'] = array(
    'name' => 'edit own field_session_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_session_time.
  $permissions['edit own field_session_time'] = array(
    'name' => 'edit own field_session_time',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own session content.
  $permissions['edit own session content'] = array(
    'name' => 'edit own session content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit terms in 4.
  $permissions['edit terms in 4'] = array(
    'name' => 'edit terms in 4',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 5.
  $permissions['edit terms in 5'] = array(
    'name' => 'edit terms in 5',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: edit terms in 6.
  $permissions['edit terms in 6'] = array(
    'name' => 'edit terms in 6',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: view field_session_day.
  $permissions['view field_session_day'] = array(
    'name' => 'view field_session_day',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_session_room.
  $permissions['view field_session_room'] = array(
    'name' => 'view field_session_room',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_session_status.
  $permissions['view field_session_status'] = array(
    'name' => 'view field_session_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_session_time.
  $permissions['view field_session_time'] = array(
    'name' => 'view field_session_time',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_session_day.
  $permissions['view own field_session_day'] = array(
    'name' => 'view own field_session_day',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_session_room.
  $permissions['view own field_session_room'] = array(
    'name' => 'view own field_session_room',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_session_status.
  $permissions['view own field_session_status'] = array(
    'name' => 'view own field_session_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_session_time.
  $permissions['view own field_session_time'] = array(
    'name' => 'view own field_session_time',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
