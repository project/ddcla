<?php
/**
 * @file
 * dcla_session.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dcla_session_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-sessions:sessions/all
  $menu_links['menu-sessions:sessions/all'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/all',
    'router_path' => 'sessions',
    'link_title' => 'All sessions',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: menu-sessions:sessions/business-and-strategy
  $menu_links['menu-sessions:sessions/business-and-strategy'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/business-and-strategy',
    'router_path' => 'sessions',
    'link_title' => 'Business & Strategy',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: menu-sessions:sessions/code-and-development
  $menu_links['menu-sessions:sessions/code-and-development'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/code-and-development',
    'router_path' => 'sessions',
    'link_title' => 'Code & Development',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
  );
  // Exported menu link: menu-sessions:sessions/commerce
  $menu_links['menu-sessions:sessions/commerce'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/commerce',
    'router_path' => 'sessions',
    'link_title' => 'Commerce',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Exported menu link: menu-sessions:sessions/design-and-user-experience
  $menu_links['menu-sessions:sessions/design-and-user-experience'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/design-and-user-experience',
    'router_path' => 'sessions',
    'link_title' => 'Design & User Experience',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
  );
  // Exported menu link: menu-sessions:sessions/performance-and-scalability
  $menu_links['menu-sessions:sessions/performance-and-scalability'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/performance-and-scalability',
    'router_path' => 'sessions',
    'link_title' => 'Performance & Scalability',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-44',
  );
  // Exported menu link: menu-sessions:sessions/picked
  $menu_links['menu-sessions:sessions/picked'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/picked',
    'router_path' => 'sessions/picked',
    'link_title' => 'Your picks',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: menu-sessions:sessions/site-building
  $menu_links['menu-sessions:sessions/site-building'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/site-building',
    'router_path' => 'sessions',
    'link_title' => 'Site Building',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-43',
  );
  // Exported menu link: menu-sessions:sessions/volunteer
  $menu_links['menu-sessions:sessions/volunteer'] = array(
    'menu_name' => 'menu-sessions',
    'link_path' => 'sessions/volunteer',
    'router_path' => 'sessions',
    'link_title' => 'Volunteer at camp',
    'options' => array(
      'attributes' => array(
        'title' => 'Want to help out at camp?',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-42',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('All sessions');
  t('Business & Strategy');
  t('Code & Development');
  t('Commerce');
  t('Design & User Experience');
  t('Performance & Scalability');
  t('Site Building');
  t('Volunteer at camp');
  t('Your picks');


  return $menu_links;
}
