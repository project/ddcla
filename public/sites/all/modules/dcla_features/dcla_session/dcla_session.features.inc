<?php
/**
 * @file
 * dcla_session.features.inc
 */

/**
 * Implements hook_views_api().
 */
function dcla_session_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function dcla_session_flag_default_flags() {
  $flags = array();
  // Exported flag: "Session voting".
  $flags['vote'] = array(
    'content_type' => 'node',
    'title' => 'Session voting',
    'global' => '0',
    'types' => array(
      0 => 'session',
    ),
    'flag_short' => 'Not Attending',
    'flag_long' => 'Add this session to my schedule',
    'flag_message' => '',
    'unflag_short' => 'Attending',
    'unflag_long' => 'Remove from my schedule',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'weight' => 0,
    'show_on_page' => 0,
    'show_on_teaser' => 0,
    'show_on_form' => 0,
    'show_contextual_link' => FALSE,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'dcla_session',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function dcla_session_node_info() {
  $items = array(
    'session' => array(
      'name' => t('Session'),
      'base' => 'node_content',
      'description' => t('Each session is a presentation at the Camp'),
      'has_title' => '1',
      'title_label' => t('Session title'),
      'help' => t('<p><strong>Submission guidelines:</strong> Hey, thanks for thinking about submitting a session.  We\'d like everyone who wants to present a session at camp be able to present, so one or two session proposals per presenter is our guideline this year. It gets really hard to avoid schedule conflicts if you submit more, and we hate to cancel sessions because of that.</p>
<p>Just so you know, we\'ve got plenty of space this year, so odds are good you\'ll be presenting this session (so no slacking off). We\'ll let you know for sure closer to the camp date.</p>

'),
    ),
  );
  return $items;
}
