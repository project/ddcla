<?php
/**
 * @file
 * dcla_sponsor.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dcla_sponsor_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:sponsors
  $menu_links['main-menu:sponsors'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'sponsors',
    'router_path' => 'sponsors',
    'link_title' => 'Sponsors',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Sponsors');


  return $menu_links;
}
