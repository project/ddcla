<?php
/**
 * @file
 * dcla_sponsor.features.inc
 */

/**
 * Implements hook_views_api().
 */
function dcla_sponsor_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function dcla_sponsor_image_default_styles() {
  $styles = array();

  // Exported image style: sponsor-large.
  $styles['sponsor-large'] = array(
    'name' => 'sponsor-large',
    'effects' => array(
      15 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => '200',
          'upscale' => 0,
        ),
        'weight' => '-10',
      ),
      13 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '480',
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => '-9',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function dcla_sponsor_node_info() {
  $items = array(
    'sponsor' => array(
      'name' => t('Sponsor'),
      'base' => 'node_content',
      'description' => t('Sponsors have detailed information about the sponsor as well as a list of attending members.'),
      'has_title' => '1',
      'title_label' => t('Sponsor name'),
      'help' => t('<strong>Instructions: </strong>Please complete this form, and be sure to upload your logo (to display on a light background).  Pick your sponsor level, submit the form, and we\'ll be in touch.'),
    ),
  );
  return $items;
}
