<?php
/**
 * @file
 * dcla_profile.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dcla_profile_user_default_permissions() {
  $permissions = array();

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: create field_user_featured.
  $permissions['create field_user_featured'] = array(
    'name' => 'create field_user_featured',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_user_gender.
  $permissions['create field_user_gender'] = array(
    'name' => 'create field_user_gender',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_user_organizer.
  $permissions['create field_user_organizer'] = array(
    'name' => 'create field_user_organizer',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_user_organizer_bio.
  $permissions['create field_user_organizer_bio'] = array(
    'name' => 'create field_user_organizer_bio',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit any attendance profile.
  $permissions['edit any attendance profile'] = array(
    'name' => 'edit any attendance profile',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: edit any main profile.
  $permissions['edit any main profile'] = array(
    'name' => 'edit any main profile',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: edit any organizer profile.
  $permissions['edit any organizer profile'] = array(
    'name' => 'edit any organizer profile',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: edit field_user_featured.
  $permissions['edit field_user_featured'] = array(
    'name' => 'edit field_user_featured',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_user_gender.
  $permissions['edit field_user_gender'] = array(
    'name' => 'edit field_user_gender',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_user_organizer.
  $permissions['edit field_user_organizer'] = array(
    'name' => 'edit field_user_organizer',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_user_organizer_bio.
  $permissions['edit field_user_organizer_bio'] = array(
    'name' => 'edit field_user_organizer_bio',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own attendance profile.
  $permissions['edit own attendance profile'] = array(
    'name' => 'edit own attendance profile',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'profile2',
  );

  // Exported permission: edit own field_user_featured.
  $permissions['edit own field_user_featured'] = array(
    'name' => 'edit own field_user_featured',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_user_gender.
  $permissions['edit own field_user_gender'] = array(
    'name' => 'edit own field_user_gender',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_user_organizer.
  $permissions['edit own field_user_organizer'] = array(
    'name' => 'edit own field_user_organizer',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_user_organizer_bio.
  $permissions['edit own field_user_organizer_bio'] = array(
    'name' => 'edit own field_user_organizer_bio',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own main profile.
  $permissions['edit own main profile'] = array(
    'name' => 'edit own main profile',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'profile2',
  );

  // Exported permission: edit own organizer profile.
  $permissions['edit own organizer profile'] = array(
    'name' => 'edit own organizer profile',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: view any attendance profile.
  $permissions['view any attendance profile'] = array(
    'name' => 'view any attendance profile',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: view any main profile.
  $permissions['view any main profile'] = array(
    'name' => 'view any main profile',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'profile2',
  );

  // Exported permission: view any organizer profile.
  $permissions['view any organizer profile'] = array(
    'name' => 'view any organizer profile',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: view field_user_featured.
  $permissions['view field_user_featured'] = array(
    'name' => 'view field_user_featured',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_user_gender.
  $permissions['view field_user_gender'] = array(
    'name' => 'view field_user_gender',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_user_organizer.
  $permissions['view field_user_organizer'] = array(
    'name' => 'view field_user_organizer',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_user_organizer_bio.
  $permissions['view field_user_organizer_bio'] = array(
    'name' => 'view field_user_organizer_bio',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own attendance profile.
  $permissions['view own attendance profile'] = array(
    'name' => 'view own attendance profile',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'profile2',
  );

  // Exported permission: view own field_user_featured.
  $permissions['view own field_user_featured'] = array(
    'name' => 'view own field_user_featured',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_user_gender.
  $permissions['view own field_user_gender'] = array(
    'name' => 'view own field_user_gender',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_user_organizer.
  $permissions['view own field_user_organizer'] = array(
    'name' => 'view own field_user_organizer',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_user_organizer_bio.
  $permissions['view own field_user_organizer_bio'] = array(
    'name' => 'view own field_user_organizer_bio',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own main profile.
  $permissions['view own main profile'] = array(
    'name' => 'view own main profile',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'profile2',
  );

  // Exported permission: view own organizer profile.
  $permissions['view own organizer profile'] = array(
    'name' => 'view own organizer profile',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'profile2',
  );

  return $permissions;
}
