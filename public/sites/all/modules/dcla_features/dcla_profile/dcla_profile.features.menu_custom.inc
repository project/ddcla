<?php
/**
 * @file
 * dcla_profile.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function dcla_profile_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-attendees.
  $menus['menu-attendees'] = array(
    'menu_name' => 'menu-attendees',
    'title' => 'Attendees',
    'description' => 'This menu provides links for filtering the Attendees view.',
  );
  // Exported menu: menu-information.
  $menus['menu-information'] = array(
    'menu_name' => 'menu-information',
    'title' => 'Information',
    'description' => 'About the camp, etc.',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About the camp, etc.');
  t('Attendees');
  t('Information');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('This menu provides links for filtering the Attendees view.');
  t('User menu');


  return $menus;
}
