<?php
/**
 * @file
 * dcla_profile.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dcla_profile_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-attendees:attendees/Arts and Music
  $menu_links['menu-attendees:attendees/Arts and Music'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Arts and Music',
    'router_path' => 'attendees',
    'link_title' => 'Arts and Music',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Corporate and Enterprise IT
  $menu_links['menu-attendees:attendees/Corporate and Enterprise IT'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Corporate and Enterprise IT',
    'router_path' => 'attendees',
    'link_title' => 'Corporate and Enterprise IT',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Drupal Service Provider
  $menu_links['menu-attendees:attendees/Drupal Service Provider'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Drupal Service Provider',
    'router_path' => 'attendees',
    'link_title' => 'Drupal Service Provider',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Drupalchix
  $menu_links['menu-attendees:attendees/Drupalchix'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Drupalchix',
    'router_path' => 'attendees',
    'link_title' => 'Drupalchix',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '1',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Education
  $menu_links['menu-attendees:attendees/Education'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Education',
    'router_path' => 'attendees',
    'link_title' => 'Education',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Freelancer
  $menu_links['menu-attendees:attendees/Freelancer'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Freelancer',
    'router_path' => 'attendees',
    'link_title' => 'Freelancer',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Government
  $menu_links['menu-attendees:attendees/Government'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Government',
    'router_path' => 'attendees',
    'link_title' => 'Government',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Hobbyist
  $menu_links['menu-attendees:attendees/Hobbyist'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Hobbyist',
    'router_path' => 'attendees',
    'link_title' => 'Hobbyist',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/Non-Profit
  $menu_links['menu-attendees:attendees/Non-Profit'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/Non-Profit',
    'router_path' => 'attendees',
    'link_title' => 'Non-Profit',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-attendees:attendees/all
  $menu_links['menu-attendees:attendees/all'] = array(
    'menu_name' => 'menu-attendees',
    'link_path' => 'attendees/all',
    'router_path' => 'attendees',
    'link_title' => 'All Attendees',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-information:information/event-organizers
  $menu_links['menu-information:information/event-organizers'] = array(
    'menu_name' => 'menu-information',
    'link_path' => 'information/event-organizers',
    'router_path' => 'information/event-organizers',
    'link_title' => 'Event Organizers',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-39',
  );
  // Exported menu link: user-menu:attendees
  $menu_links['user-menu:attendees'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'attendees',
    'router_path' => 'attendees',
    'link_title' => 'Attendees',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('All Attendees');
  t('Arts and Music');
  t('Attendees');
  t('Corporate and Enterprise IT');
  t('Drupal Service Provider');
  t('Drupalchix');
  t('Education');
  t('Event Organizers');
  t('Freelancer');
  t('Government');
  t('Hobbyist');
  t('Non-Profit');


  return $menu_links;
}
